﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;

namespace testCSV
{

    [TestFixture]
    public class UntitledTestCase
    {
        private InternetExplorerDriver driver;
        private StringBuilder verificationErrors;
        private string baseURL;
        private bool acceptNextAlert = true;

        [SetUp]
        public void SetupTest()
        {
            driver = new InternetExplorerDriver();
            baseURL = "https://www.katalon.com/";
            verificationErrors = new StringBuilder();
        }


        [TearDown]
        public void TeardownTest()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
            NUnit.Framework.Assert.AreEqual("", verificationErrors.ToString());
        }

        [Test]
        public void TheUntitledTestCaseTest()
        {
            driver.Navigate().GoToUrl("http://www.google.fr");
            driver.FindElement(By.Name("q")).SendKeys("rien");

            // lance la recherche
            driver.FindElement(By.Name("q")).Submit();

        }
        [Test]
        public void test2()
        {
            driver.Navigate().GoToUrl("http://www.google.fr");
            driver.FindElement(By.Name("q")).SendKeys("tout");

            // lance la recherche
            driver.FindElement(By.Name("q")).Submit();

        }
        private bool IsElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        private bool IsAlertPresent()
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }

        private string CloseAlertAndGetItsText()
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (acceptNextAlert)
                {
                    alert.Accept();
                }
                else
                {
                    alert.Dismiss();
                }
                return alertText;
            }
            finally
            {
                acceptNextAlert = true;
            }
        }
    }
}
